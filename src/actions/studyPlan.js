import Swal from 'sweetalert2';
import { setError } from './ui';
import { types } from '../types/types';
import {
  createStudyPlanApi,
  getAllStudyPlansApi,
  updateStudyPlanApi,
} from '../helpers/studyPlan';

export const listAll = (studyPlans) => ({
  type: types.studyPlanListAll,
  payload: studyPlans,
});

export const studyPlanListAll = () => {
  return async (dispatch) => {
    try {
      const resp = await getAllStudyPlansApi();

      if (resp.code === 200) {
        dispatch(listAll(resp.data));
      } else {
        dispatch(setError(resp.detail));
        Swal.fire('Error', resp.detail, 'error');
      }
    } catch (error) {
      dispatch(setError(error.detail));
      Swal.fire('Error', error.detail, 'error');
    }
  };
};

export const addNewStudyPlan = (studyPlan) => ({
  type: types.studyPlanAddNew,
  payload: studyPlan,
});

export const studyPlanStartAddNew = (event) => {
  return async (dispatch) => {
    try {
      Swal.fire({
        title: 'Guardando...',
        text: 'Espere...',
        allowOutsideClick: false,
        allowEscapeKey: false,
        didOpen: () => {
          Swal.showLoading();
        },
      });
      const resp = await createStudyPlanApi(event);
      if (resp.code === 200) {
        dispatch(addNewStudyPlan(resp));
        dispatch(studyPlanListAll());
        Swal.close();
        Swal.fire('Guardado', resp.statusText, 'success');
      } else {
        dispatch(setError(resp.detail));
        Swal.fire('Error', resp.detail, 'error');
      }
    } catch (error) {
      dispatch(setError(error.detail));
      Swal.fire('Error', error.detail, 'error');
    }
  };
};

export const studyPlanSetActive = (studyPlan) => ({
  type: types.studyPlanSetActive,
  payload: studyPlan,
});

export const studyPlanClearActive = () => ({
  type: types.studyPlanClearActive,
});

export const updateStudyPlan = (studyPlan) => ({
  type: types.studyPlanUpdate,
  payload: studyPlan,
});

export const studyPlanStartUpdate = (studyPlan) => {
  return async (dispatch) => {
    try {
      Swal.fire({
        title: 'Modificando...',
        text: 'Espere...',
        allowOutsideClick: false,
        allowEscapeKey: false,
        didOpen: () => {
          Swal.showLoading();
        },
      });
      const resp = await updateStudyPlanApi(studyPlan);
      if (resp.code === 200) {
        dispatch(updateStudyPlan(resp));
        dispatch(studyPlanListAll());
        Swal.close();
        Swal.fire('Guardado', resp.statusText, 'success');
      } else {
        dispatch(setError(resp.detail));
        Swal.fire('Error', resp.detail, 'error');
      }
    } catch (error) {
      dispatch(setError(error.detail));
      Swal.fire('Error', error.detail, 'error');
    }
  };
};
