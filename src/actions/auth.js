import Swal from 'sweetalert2';
import { types } from '../types/types';
import { firebase, googleAuthProvider } from '../firebase/firebaseConfig';
import { finishLoading, startLoading } from './ui';

export const login = (uid, displayName) => ({
  type: types.login,
  payload: {
    uid,
    displayName,
  },
});

export const startGoogleLogin = () => (dispatch) => {
  return firebase
    .auth()
    .signInWithPopup(googleAuthProvider)
    .then(({ user }) => {
      dispatch(login(user.uid, user.displayName));
    })
    .catch((e) => {
      console.log(e);
    });
};

export const startLoginEmailPassword = (email, password) => (dispatch) => {
  dispatch(startLoading());

  return firebase
    .auth()
    .signInWithEmailAndPassword(email, password)
    .then(({ user }) => {
      dispatch(login(user.uid, user.displayName));
      dispatch(finishLoading());
    })
    .catch(() => {
      dispatch(finishLoading());
      Swal.fire('Error', 'Usuario o pasword invalida', 'error');
    });
};

export const logout = () => ({
  type: types.logout,
});

export const startLogout = () => async (dispatch) => {
  await firebase.auth().signOut();
  dispatch(logout());
};
