import React, { useState } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { useForm } from '../../hooks/useForm';
import {
  studyPlanStartAddNew,
  studyPlanStartUpdate,
  studyPlanClearActive,
} from '../../actions/studyPlan';
import '../../styles/form.css';
import { CustomModal } from '../modal/CustomModal';
import { setError, removeError, closeModal } from '../../actions/ui';

export const StudyPlanModal = () => {
  const dispatch = useDispatch();
  const { msgError } = useSelector((state) => state.ui);
  const { active } = useSelector((state) => state.studyPlan);
  const initialState = active || {
    code: '',
    name: '',
    description: '',
  };

  const [formValues, handleInputChange, reset] = useForm(initialState);

  const { code, name, description } = formValues;
  const [typeError, setTypeError] = useState('');

  const formValid = () => {
    // TODO: validar codigo unico
    console.log('entre');
    if (code.trim().length === 0) {
      dispatch(setError('Codigo es requerido'));
      setTypeError('code');
      return false;
    }

    if (name.trim().length === 0) {
      dispatch(setError('Nombre es requerido'));
      setTypeError('name');
      console.log(msgError);
      return false;
    }
    dispatch(removeError());
    setTypeError(null);
    return true;
  };

  const handleClose = () => {
    reset();
    dispatch(removeError());
    dispatch(studyPlanClearActive());
    dispatch(closeModal());
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(formValues);
    if (formValid()) {
      if (active) {
        dispatch(studyPlanStartUpdate(formValues));
        dispatch(studyPlanClearActive());
      } else {
        dispatch(studyPlanStartAddNew(formValues));
      }
      handleClose();
    }
  };

  return (
    <CustomModal onClose={handleClose}>
      <div
        id="study-plan-modal"
        className="rounded flex flex-wrap flex-col items-center justify-center h-full"
      >
        <div className="form-header">
          <h1 className="form-title">
            {active ? 'Modificar  Plan' : 'Crear  Plan'}
          </h1>
        </div>
        <form className="flex flex-col mb-3" onSubmit={handleSubmit}>
          <div className="flex flex-col">
            <input
              autoComplete="off"
              value={code}
              onChange={handleInputChange}
              className={`form-input mx-3 mb-0 p-0 ${
                msgError && typeError === 'code' ? 'border-red-600' : ''
              }`}
              name="code"
              type="text"
              placeholder="Codigo"
            />
            {msgError && typeError === 'code' && (
              <span className="mx-3 form-error">{msgError}</span>
            )}
          </div>
          <input
            autoComplete="off"
            name="name"
            value={name}
            onChange={handleInputChange}
            className="form-input mx-3"
            type="text"
            placeholder="Nombre"
          />
          {msgError && typeError === 'name' && (
            <span className="mx-3 form-error">{msgError}</span>
          )}
          <textarea
            value={description}
            name="description"
            className="form-textarea mx-3 "
            placeholder="Descripcion"
            onChange={handleInputChange}
          />

          <div className="flex justify-between mb-2 mx-3 my-4">
            <button
              type="button"
              onClick={handleClose}
              className="form-button button-close"
            >
              Cerrar
            </button>
            <button className="form-button button-save" type="submit">
              Guardar
            </button>
          </div>
        </form>
      </div>
    </CustomModal>
  );
};
