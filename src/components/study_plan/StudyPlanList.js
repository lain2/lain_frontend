import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { studyPlanListAll, studyPlanSetActive } from '../../actions/studyPlan';
import { TrashIcon, EyeIcon, PencilIcon } from '../ui/Icons';
import { openModal } from '../../actions/ui';

export const StudyPlanList = () => {
  const dispatch = useDispatch();
  const { studyPlans } = useSelector((state) => state.studyPlan);

  useEffect(() => {
    dispatch(studyPlanListAll());
  }, [dispatch]);

  const handleOpen = () => {
    console.log('entre');
    dispatch(openModal());
  };

  const handleActiveElement = (studyPlan) => {
    dispatch(studyPlanSetActive(studyPlan));
    dispatch(openModal());
  };

  return (
    <div className="p-2 m-2">
      <h1 className="">StudyPlanList</h1>
      <button
        className="w-full sm:hidden bg-opacity-25 bg-green-500 rounded-lg px-6 py-2 hover:bg-green-600 font-bold"
        onClick={handleOpen}
        type="button"
      >
        {' '}
        +
      </button>
      <table className="table-base">
        <thead>
          <tr className="table-header">
            <th className="column-left">Codigo</th>
            <th className="column-left">Nombre</th>
            <th className="column-left">Description</th>
            <th className="column-center">Estado</th>
            <th className="column-center">
              <button
                className="bg-opacity-25 bg-green-500 rounded-lg px-6 py-2 hover:bg-green-600 font-bold"
                onClick={handleOpen}
                type="button"
              >
                {' '}
                +
              </button>
            </th>
          </tr>
        </thead>
        <tbody className="table-body ">
          {studyPlans?.map((sp) => (
            <tr
              key={sp.id}
              className="trow flex flex-col flex-no-wrap sm:table-row sm:mb-0"
            >
              <td className="tdesc column-left font-medium">{sp.code}</td>
              <td className="tdesc column-left ">{sp.name}</td>
              <td className="tdesc column-left">{sp.description}</td>
              <td className="tdesc column-center">
                {sp.status ? (
                  <span className="state-active">Active</span>
                ) : (
                  <span className="state-disabled">Disabled</span>
                )}
              </td>
              <td className="column-center">
                <div className="flex item-center justify-center">
                  <EyeIcon />
                  <PencilIcon onClick={() => handleActiveElement(sp)} />
                  <TrashIcon />
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};
