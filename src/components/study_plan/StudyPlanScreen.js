import React from 'react';
import { useSelector } from 'react-redux';
import { StudyPlanModal } from './StudyPlanModal';
import { StudyPlanList } from './StudyPlanList';

export const StudyPlanScreen = () => {
  const { modalOpen } = useSelector((state) => state.ui);

  return (
    <>
      <h1>StudyPlanScreen</h1>

      {modalOpen && <StudyPlanModal />}
      <StudyPlanList />
    </>
  );
};
