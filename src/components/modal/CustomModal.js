import React from 'react';
import { useSelector } from 'react-redux';
import Modal from 'react-modal';

const enviroment = process.env.NODE_ENV;
if (enviroment !== 'test') {
  Modal.setAppElement('#modal');
}

export const CustomModal = ({ children, onClose }) => {
  const { modalOpen } = useSelector((state) => state.ui);

  return (
    <Modal
      isOpen={modalOpen}
      // onAfterOpen={afterOpenModal}
      onRequestClose={onClose}
      closeTimeoutMS={200}
      className="modal"
      overlayClassName="modal-fondo"
      ariaHideApp={process.env.NODE_ENV !== 'test'}
    >
      {children}
    </Modal>
  );
};
