import React, { useState } from 'react';
import validator from 'validator';
import { useDispatch, useSelector } from 'react-redux';
import { startGoogleLogin, startLoginEmailPassword } from '../../actions/auth';
import { setError, removeError } from '../../actions/ui';
import { useForm } from '../../hooks/useForm';

export const LoginScreen = () => {
  const urlLogo =
    'https://upload.wikimedia.org/wikipedia/commons/5/53/Google_%22G%22_Logo.svg';

  const dispatch = useDispatch();
  const { msgError, loading } = useSelector((state) => state.ui);

  const [formValues, handleInputChange] = useForm({
    email: '',
    password: '',
  });

  const { email, password } = formValues;

  const [typeError, setTypeError] = useState('');
  const isFormValid = () => {
    if (email.trim().length === 0) {
      dispatch(setError('Email es requerido'));
      setTypeError('msg');
      return false;
    }

    if (!validator.isEmail(email)) {
      dispatch(setError('Email no valido'));
      setTypeError('msg');
      return false;
    }

    if (password.trim().length === 0) {
      dispatch(setError('Password es requerido'));
      setTypeError('password');
      return false;
    }

    dispatch(removeError());

    return true;
  };

  const handleLogin = (e) => {
    e.preventDefault();
    if (isFormValid()) {
      dispatch(startLoginEmailPassword(email, password));
    }
  };

  const handleGoogleLogin = () => {
    dispatch(startGoogleLogin());
  };

  return (
    <div className="flex items-center justify-center h-full">
      <div className="md:flex max-w-sm md:max-w-3xl ">
        <div className="bg-custom-first md:w-1/2 p-8 rounded-2xl md:rounded-l-2xl md:rounded-none shadow-2xl  ">
          <h1 className="text-white text-3xl text-center mb-4">Login</h1>
          <form onSubmit={handleLogin} className="text-gray-400">
            <div className="my-3 ">
              <input
                autoComplete="off"
                name="email"
                types="text"
                placeholder="Email"
                className={`${
                  msgError && typeError === 'msg'
                    ? 'border-red-600'
                    : 'border-custom-fifth'
                } border bg-custom-fouth rounded w-full py-2 px-4 outline-none focus:shadow-outline`}
                value={email}
                onChange={handleInputChange}
              />
              {msgError && typeError === 'msg' && (
                <p className="text-red-600">{msgError}</p>
              )}
            </div>
            <div className="my-3">
              <input
                type="password"
                name="password"
                placeholder="Password"
                className={`${
                  msgError && typeError === 'password'
                    ? 'border-red-600'
                    : 'border-custom-fifth'
                } border bg-custom-fouth rounded w-full py-2 px-4 outline-none focus:shadow-outline`}
                value={password}
                onChange={handleInputChange}
              />
              {msgError && typeError === 'password' && (
                <p className="text-red-600">{msgError}</p>
              )}
            </div>
            <div className="my-3 flex justify-between">
              <button
                type="submit"
                className="rounded w-1/2 py-2 px-4 bg-purple-600 text-white hover:bg-purple-800"
                disabled={loading}
                id="login-submit"
              >
                Ingresar
              </button>
              <button
                type="button"
                className="rounded w-1/2 ml-2 py-2 px-4 bg-custom-blue text-black hover:bg-blue-800 flex justify-start items-center"
                id="google-btn"
                onClick={handleGoogleLogin}
              >
                <div className="bg-white rounded px-2 py-2">
                  <img
                    src={urlLogo}
                    alt=""
                    className="w-6 mr-0 bg-white rounded-full"
                  />
                </div>
                <p className="ml-2 text-white">Google</p>
              </button>
            </div>
          </form>
        </div>
        <div className="hidden md:block w-1/2 relative shadow-2xl">
          <img
            className="w-full h-full object-cover rounded-r-2xl"
            src="https://images.unsplash.com/photo-1477346611705-65d1883cee1e?dpr=0.800000011920929&auto=format&fit=crop&w=1199%E2%94%82&h=800&q=80&cs=tinysrgb&crop="
            alt=""
          />
        </div>
      </div>
    </div>
  );
};
