import React from 'react';
import { useDispatch } from 'react-redux';
import { startLogout } from '../../actions/auth';

export const Task = () => {
  const dispatch = useDispatch();
  const handleLogout = () => {
    dispatch(startLogout());
  };

  return (
    <>
      <div className="container">
        <h1 className="flex text-black text-4xl justify-center">Bienvenido</h1>
        <button
          type="button"
          className="flex justify-center  bg-purple-600 p-2 text-xl rounded-2xl"
          onClick={handleLogout}
        >
          Logout
        </button>
      </div>
    </>
  );
};
