import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { PrivateRoute } from './PrivateRoute';
import { PublicRoute } from './PublicRoute';
import AuthRouter from './AuthRouter';
import { Task } from '../components/tasks/Task';
import { firebase } from '../firebase/firebaseConfig';
import { login } from '../actions/auth';
import { StudyPlanScreen } from '../components/study_plan/StudyPlanScreen';

const AppRouter = () => {
  const [checking, setChecking] = useState(true);
  const dispatch = useDispatch();
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  useEffect(() => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user?.uid) {
        dispatch(login(user.uid, user.displayName));
        setIsLoggedIn(true);
      } else {
        setIsLoggedIn(false);
      }
      setChecking(false);
    });
  }, [dispatch, setChecking]);

  if (checking) {
    return <h1>Espere ...</h1>;
  }
  return (
    <Router>
      <div>
        <Switch>
          <PublicRoute
            path="/auth"
            isAuthenticated={isLoggedIn}
            component={AuthRouter}
          />
          <PublicRoute
            path="/study-plan"
            isAuthenticated={isLoggedIn}
            component={StudyPlanScreen}
          />
          <PrivateRoute
            exact
            path="/"
            isAuthenticated={isLoggedIn}
            component={Task}
          />
        </Switch>
      </div>
    </Router>
  );
};

export default AppRouter;
