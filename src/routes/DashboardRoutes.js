import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { StudyPlanScreen } from '../components/study_plan/StudyPlanScreen';

export const DashboardRoutes = () => {
  return (
    <>
      <Switch>
        <Route exact path="/study-plan" component={StudyPlanScreen} />
      </Switch>
    </>
  );
};
