import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { LoginScreen } from '../components/auth/LoginScreen';

const AuthRouter = () => {
  return (
    <div>
      <div className="bg-grey-700 h-screen">
        <Switch>
          <Route path="/auth/login" component={LoginScreen} />
          {/* <Route path="/auth/register" component={RegisterScreen} /> */}
          <Redirect to="/auth/login" />
        </Switch>
      </div>
    </div>
  );
};

export default AuthRouter;
