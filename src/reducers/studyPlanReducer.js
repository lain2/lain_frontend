import { types } from '../types/types';

const initialState = {
  studyPlans: [],
};

export const studyPlanReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.studyPlanAddNew:
      return {
        ...state,
        studyPlans: [...state.studyPlans, action.payload],
      };
    case types.studyPlanListAll:
      console.log(state);
      console.log(action);
      return {
        ...state,
        studyPlans: [...action.payload],
      };
    case types.studyPlanSetActive:
      return {
        ...state,
        active: action.payload,
      };
    case types.studyPlanClearActive:
      return {
        ...state,
        active: null,
      };
    case types.studyPlanUpdate:
      return {
        ...state,
        studyPlans: state.studyPlans.map((s) =>
          s.id === action.payload.id ? action.payload : s
        ),
      };
    default:
      return state;
  }
};
