import { combineReducers } from 'redux';
import { authReducer } from './authReducer';
import { studyPlanReducer } from './studyPlanReducer';
import { uiReducer } from './uiReducer';

export const rootReducer = combineReducers({
  auth: authReducer,
  ui: uiReducer,
  studyPlan: studyPlanReducer,
});
