export const types = {
  login: '[AUTH] Login',
  logout: '[AUTH] Logout',

  uiSetError: '[UI] Set error',
  uiRemoveError: '[UI] Remove error',
  uiStartLoading: '[UI] Start loading',
  uiFinishLoading: '[UI] Finish loading',
  uiOpenModal: '[UI] Open Modal',
  uiCloseModal: '[UI] Close Modal',

  studyPlanStartNew: '[SP] Start add new',
  studyPlanAddNew: '[SP] Add new',
  studyPlanListAll: '[SP] List all',
  studyPlanStartUpdate: '[SP] Start update',
  studyPlanUpdate: '[SP] Update study plan',
  studyPlanSetActive: '[SP] Set active',
  studyPlanClearActive: '[SP] Clear active study plan',
};
