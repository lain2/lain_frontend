import React from 'react';
import ReactDOM from 'react-dom';
import './styles/style.css';
import LainApp from './LainApp';

ReactDOM.render(<LainApp />, document.getElementById('root'));
