export const createStudyPlanApi = (post) => {
  const url = `${process.env.BASE_URL}/studyPlan`;
  // const url = 'http://localhost:8000/api/v1/developer/study-plan';
  const request = {
    method: 'POST',
    body: JSON.stringify(post),
    headers: { 'Content-Type': 'application/json; charset=UTF-8' },
  };

  return fetch(url, request).then(async (response) => {
    const data = await response.json();
    return {
      ...data,
      code: response.status,
    };
  });
};

export const getAllStudyPlansApi = async () => {
  const url = `${process.env.REACT_APP_API_URL}/study-plan`;
  // const url = 'http://localhost:8000/api/v1/developer/study-plan';
  const request = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json; charset=UTF-8' },
  };
  const response = await fetch(url, request);
  const data = await response.json();

  console.log({
    ...data,
    code: response.status,
  });
  return {
    data,
    code: response.status,
  };
};

export const updateStudyPlanApi = (studyPlan) => {
  const url = `${process.env.REACT_APP_API_URL}/study-plan`;
  // const url = 'http://localhost:8000/api/v1/developer/study-plan';
  const request = {
    method: 'PUT',
    body: JSON.stringify(studyPlan),
    headers: { 'Content-Type': 'application/json; charset=UTF-8' },
  };

  return fetch(url, request).then(async (response) => {
    const data = await response.json();
    return {
      ...data,
      code: response.status,
    };
  });
};
