import React from 'react';
import { Provider } from 'react-redux';

import store from './store/store';
import AppRouter from './routes/AppRouter';

const LainApp = () => (
  <Provider store={store}>
    <AppRouter />
  </Provider>
);

export default LainApp;
