import {
  removeError,
  setError,
  startLoading,
  finishLoading,
  openModal,
  closeModal,
} from '../../actions/ui';
import { types } from '../../types/types';

describe('Test ui-actions', () => {
  test('should generate setError action', () => {
    const action = setError('Error!!!');

    expect(action).toEqual({ type: types.uiSetError, payload: 'Error!!!' });
  });

  test('should generate removeError action', () => {
    const removeErrorAction = removeError();

    expect(removeErrorAction).toEqual({
      type: types.uiRemoveError,
    });
  });

  test('should generate startLoading action', () => {
    const startLoadingAction = startLoading();

    expect(startLoadingAction).toEqual({
      type: types.uiStartLoading,
    });
  });

  test('should generate finishLoading action', () => {
    const finishLoadingAction = finishLoading();

    expect(finishLoadingAction).toEqual({
      type: types.uiFinishLoading,
    });
  });

  test('should generate openModal action', () => {
    const openModalAction = openModal();

    expect(openModalAction).toEqual({
      type: types.uiOpenModal,
    });
  });

  test('should generate closeModal action', () => {
    const closeModalAction = closeModal();

    expect(closeModalAction).toEqual({
      type: types.uiCloseModal,
    });
  });
});
