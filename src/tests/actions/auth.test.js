import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import {
  login,
  logout,
  startLogout,
  startLoginEmailPassword,
} from '../../actions/auth';
import { types } from '../../types/types';
import '@testing-library/jest-dom';

const middlewares = [thunk];
const mockStore = configureStore(middlewares);
const initState = {};
let store = mockStore(initState);

describe('Test Actions Auth', () => {
  beforeEach(() => {
    store = mockStore(initState);
  });

  test('login deben de crear la accion respectiva', () => {
    const uid = 'ABC123';
    const displayName = 'Texier';

    const loginAction = login(uid, displayName);

    expect(loginAction).toEqual({
      type: types.login,
      payload: {
        uid,
        displayName,
      },
    });
  });

  test('logout deben de crear la accion respectiva', () => {
    const logoutAction = logout();

    expect(logoutAction).toEqual({
      type: types.logout,
    });
  });

  test('debe realizar el start logout con firebase ', async () => {
    await store.dispatch(startLogout());
    const actions = store.getActions();
    expect(actions[0]).toEqual({ type: types.logout });
  });

  test('debe realizar el startLogin con email y password', async () => {
    await store.dispatch(startLoginEmailPassword('test@testing.com', '123456'));
    const actions = store.getActions();
    expect(actions[1]).toEqual({
      type: types.login,
      payload: {
        uid: '6WSPqW23HWdton1YG2jUiR7dRDU2',
        displayName: null,
      },
    });
  });
});
