import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { types } from '../../types/types';
import {
  studyPlanStartAddNew,
  addNewStudyPlan,
  updateStudyPlan,
  studyPlanListAll,
} from '../../actions/studyPlan';

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

const initState = {
  ui: {
    msgError: null,
    loading: false,
    modalOpen: true,
  },
  studyPlan: {
    studyPlans: [],
  },
};

jest.mock('../../helpers/studyPlan', () => ({
  getAllStudyPlansApi: jest.fn(() => ({
    code: 200,
    data: [
      {
        id: '123',
        code: '0012',
        name: 'lic. en sistemas',
        description: 'ejemplo',
        status: true,
      },
    ],
  })),
}));

let store = mockStore(initState);
window.scrollTo = jest.fn();

describe('Test study plan actions', () => {
  beforeEach(() => {
    store = mockStore(initState);
  });

  test('should generate a new studyPlanStartAddNew action', async () => {
    const studyPlan = {
      code: '0012',
      name: 'lic. en sistemas',
      description: 'ejemplo',
    };

    await store.dispatch(studyPlanStartAddNew(studyPlan));
    const actions = store.getActions();

    expect(actions.length).not.toEqual(0);
  });

  test('should generate a new addNewStudyPlan action', () => {
    const studyPlan = {
      code: '0012',
      name: 'lic. en sistemas',
      description: 'ejemplo',
    };

    const addNewStudyPlanAction = addNewStudyPlan(studyPlan);

    expect(addNewStudyPlanAction).toEqual({
      type: types.studyPlanAddNew,
      payload: studyPlan,
    });
  });

  test('should load a list of studyPlans', async () => {
    await store.dispatch(studyPlanListAll());

    const actions = store.getActions();

    expect(actions[0]).toEqual({
      type: types.studyPlanListAll,
      payload: expect.any(Array),
    });
  });

  test('should generate a new addUpdateStudyPlan action', () => {
    const studyPlan = {
      code: '0012',
      name: 'lic. en sistemas',
      description: 'ejemplo',
    };

    const updateStudyPlanAction = updateStudyPlan(studyPlan);

    expect(updateStudyPlanAction).toEqual({
      type: types.studyPlanUpdate,
      payload: studyPlan,
    });
  });
});
