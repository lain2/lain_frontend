import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';

import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { StudyPlanModal } from '../../../components/study_plan/StudyPlanModal';

jest.mock('../../../actions/studyPlan', () => ({
  addNewStudyPlan: jest.fn(),
  studyPlanStartAddNew: jest.fn(),
  studyPlanStartUpdate: jest.fn(),
}));

const initialState = {
  ui: {
    msgError: '',
    modalOpen: true,
  },
  studyPlan: {
    active: null,
  },
};

const middlewares = [thunk];
const mockStore = configureStore(middlewares);
let store;
let wrapper;

describe('Test component <StudyPlanModal/>', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  test('should show the modal', () => {
    store = mockStore(initialState);
    store.dispatch = jest.fn();
    wrapper = mount(
      <Provider store={store}>
        <StudyPlanModal />
      </Provider>
    );
    expect(wrapper.find('Modal').prop('isOpen')).toBe(true);
  });

  test('should show create modal', () => {
    console.log(initialState);
    store = mockStore(initialState);
    store.dispatch = jest.fn();
    wrapper = mount(
      <Provider store={store}>
        <StudyPlanModal />
      </Provider>
    );
    expect(wrapper.find('h1').text()).toEqual('Crear  Plan');
  });

  test('should show update modal', () => {
    initialState.studyPlan.active = {
      code: '0001',
    };
    console.log(initialState);
    store = mockStore(initialState);
    store.dispatch = jest.fn();
    wrapper = mount(
      <Provider store={store}>
        <StudyPlanModal />
      </Provider>
    );

    expect(wrapper.find('h1').text()).toEqual('Modificar  Plan');
  });
});
