import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';

import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { StudyPlanList } from '../../../components/study_plan/StudyPlanList';

jest.mock('../../../actions/studyPlan', () => ({
  studyPlanListAll: jest.fn(),
  listAll: jest.fn(),
}));

const initState = {
  ui: {
    msgError: '',
    modalOpen: false,
  },
  studyPlan: {
    studyPlans: [
      {
        id: '001',
        code: '0001',
        name: 'Lic. en sistemas',
        description: 'descripcion',
        status: true,
      },
    ],
  },
};

const middlewares = [thunk];
const mockStore = configureStore(middlewares);
const store = mockStore(initState);
store.dispatch = jest.fn();

const wrapper = mount(
  <Provider store={store}>
    <StudyPlanList />
  </Provider>
);

describe('Test component <StudyPlanModal />', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  test('should show the list', () => {
    expect(wrapper.find('table').exists());
  });

  test('should show the list', () => {
    expect(wrapper.find('table').exists());
  });

  test('should show a element', () => {
    expect(wrapper.find('td').exists());
    expect(wrapper.find('.status-active').exists());
  });
});
