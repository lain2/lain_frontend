import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { MemoryRouter } from 'react-router-dom';
import { types } from '../../../types/types';
import { LoginScreen } from '../../../components/auth/LoginScreen';
import {
  startGoogleLogin,
  startLoginEmailPassword,
} from '../../../actions/auth';

jest.mock('../../../actions/auth', () => ({
  startGoogleLogin: jest.fn(),
  startLoginEmailPassword: jest.fn(),
}));

const middlewares = [thunk];
const mockStore = configureStore(middlewares);
const initState = {
  auth: {},
  ui: {
    loading: false,
    msgError: null,
  },
};

describe('Test en <LoginScreen/>', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  test('debe de mostrarse login correctamente', () => {
    const store = mockStore(initState);
    store.dispatch = jest.fn();
    const wrapper = mount(
      <Provider store={store}>
        <MemoryRouter>
          <LoginScreen />
        </MemoryRouter>
      </Provider>
    );
    expect(wrapper).toMatchSnapshot();
  });

  test('debe de iniciar login de google', () => {
    const store = mockStore(initState);
    store.dispatch = jest.fn();
    const wrapper = mount(
      <Provider store={store}>
        <MemoryRouter>
          <LoginScreen />
        </MemoryRouter>
      </Provider>
    );
    wrapper.find('#google-btn').prop('onClick')();
    expect(startGoogleLogin).toHaveBeenCalled();
  });

  test('debe de iniciar login con email y password', () => {
    const store = mockStore(initState);
    store.dispatch = jest.fn();
    const wrapper = mount(
      <Provider store={store}>
        <MemoryRouter>
          <LoginScreen />
        </MemoryRouter>
      </Provider>
    );
    const emailField = wrapper.find('input[name="email"]');
    emailField.simulate('change', {
      target: {
        value: 'test@test.com',
        name: 'email',
      },
    });
    const passwordField = wrapper.find('input[name="password"]');
    passwordField.simulate('change', {
      target: {
        value: '123456',
        name: 'password',
      },
    });
    wrapper.find('form').prop('onSubmit')({
      preventDefault() {},
    });
    expect(startLoginEmailPassword).toHaveBeenCalledWith(
      'test@test.com',
      '123456'
    );
  });

  test('debe de generar error al mandar mail vacio', () => {
    const store = mockStore(initState);
    const wrapper = mount(
      <Provider store={store}>
        <MemoryRouter>
          <LoginScreen />
        </MemoryRouter>
      </Provider>
    );
    wrapper.find('#login-submit').simulate('submit', {
      preventDefault() {},
    });
    const actions = store.getActions();

    expect(actions[0]).toEqual({
      type: types.uiSetError,
      payload: 'Email es requerido',
    });
  });

  test('debe de generar error al mandar mail en mal formato', () => {
    const store = mockStore(initState);
    const wrapper = mount(
      <Provider store={store}>
        <MemoryRouter>
          <LoginScreen />
        </MemoryRouter>
      </Provider>
    );
    const emailField = wrapper.find('input[name="email"]');
    emailField.simulate('change', {
      target: {
        value: 'test',
        name: 'email',
      },
    });
    wrapper.find('#login-submit').simulate('submit', {
      preventDefault() {},
    });
    const actions = store.getActions();

    expect(actions[0]).toEqual({
      type: types.uiSetError,
      payload: 'Email no valido',
    });
  });

  test('debe de generar error al ingregar sin password', () => {
    const store = mockStore(initState);
    const wrapper = mount(
      <Provider store={store}>
        <MemoryRouter>
          <LoginScreen />
        </MemoryRouter>
      </Provider>
    );
    const emailField = wrapper.find('input[name="email"]');
    emailField.simulate('change', {
      target: {
        value: 'test@test.com',
        name: 'email',
      },
    });
    wrapper.find('#login-submit').simulate('submit', {
      preventDefault() {},
    });
    const actions = store.getActions();

    expect(actions[0]).toEqual({
      type: types.uiSetError,
      payload: 'Password es requerido',
    });
  });
});
