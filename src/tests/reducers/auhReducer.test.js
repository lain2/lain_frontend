import { authReducer } from '../../reducers/authReducer';
import { types } from '../../types/types';

describe('Test Reducer', () => {
  test('debe de realizar el login', () => {
    const initState = {};
    const action = {
      type: types.login,
      payload: {
        uid: '123',
        displayName: 'Texier',
      },
    };
    const state = authReducer(initState, action);
    expect(state).toEqual({
      uid: '123',
      name: 'Texier',
    });
  });

  test('debe de realizar el logout', () => {
    const initState = {};
    const action = {
      type: types.logout,
    };
    const state = authReducer(initState, action);
    expect(state).toEqual({});
  });

  test('no deberia hacer cambios en el state', () => {
    const initState = {
      uid: '123',
      name: 'Texier',
    };
    const action = {};
    const state = authReducer(initState, action);
    expect(state).toEqual(initState);
  });
});
